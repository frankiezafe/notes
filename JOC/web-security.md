# sécurité et vie privée

notes pour les JOC à mons

## bases

pas de formule magique pour protéger ses données, mais une posture: s'interroger sur les intérets de ceux qui fournissent le programme ou le service, quelle est leur idéologie, comment gagnent-ils de l'argent
la pire attitude: passivité et conformation sociale, c'est ce sur quoi jouent tous les sociétés: faire de leur solution un lieu commun, que leur produit devienne la norme

- qui utilise "stylo bille"? bic est une société privée, à chaque utilisation du terme, on renforce la présence de la marque dans la société!
- facebook, twitter sont devenu des mots courants, compris de tous, au détriment de "poster" ou "réseau social"
un des champ de bataille est celui de la convention sociale passant par les mots -> faire apparaitre d'autres termes dans l'imaginaire collectif est tout aussi important que d' utiliser les réseaux libre / alternatifs
rester en contact avec le reste de la population, ne pas s'enfermer dans des sectes 
valable à tous les niveaux :)

la première règle de sécurité sur un ordi: n'installer que des programmes "clean", c'est-à-dire des programmes installés à partir d'une source officielle
ce qui amène tout de suite la question du soft piraté! et particulièrement en ce qui concerne l'OS!

utiliser du soft cracké, risques

- http://bitdefender.com/tech-assist/self-help/dangers-of-using-pirated-software.html
- https://www.getpcapps.com/software/operating-systems/risk-factors-pirated-windows-10-os.html
- https://www.thewindowsclub.com/consequences-risks-pirated-counterfeit-software

windows est un des OS les plus utilisés, et donc le plus attaqué par les hackers

- https://www.w3schools.com/browsers/browsers_os.asp

la deuxième règle: configurer vos OS et programmes

les réglages par défaut des programmes, notamment en ce qui concerne la collecte de données personnelles, sont souvent en défaveur de l'utilisateur
aller désactiver tout ce qui concerne l'envoi de données automatiques vers le producteur du logiciel, surtout quand il s'agit de logiciel proriétaire!

## modèle commercial

toujours se poser la question de la manière dont le producteur du programme ou service en ligne finance son activité

### les OS

les OS propriétaires (Windows, OSX, iOS) ne sont PAS gratuit: le prix de la license est inclus dans le prix de vente du hardware!
![](license-windows.png)

- apple : https://9to5mac.com/2018/05/01/apple-earnings-fy18-q2/
"Apple announces Q2 2018 revenue of $61.1b: 52.2m iPhones, 9.1m iPads, 4.07m Macs"

- windows : https://venturebeat.com/2018/07/19/microsoft-reports-30-1-billion-in-q4-2018-revenue-azure-up-89-surface-up-25-and-windows-up-7/
"Microsoft reports $30.1 billion in Q4 2018 revenue: Azure up 89%, Surface up 25%, and Windows up 7%"

- android : https://www.androidauthority.com/google-play-store-revenue-2018-944201/
"Google Play Store earned $24.8 billion in 2018"

### les plateformes

- twitter : https://en.wikipedia.org/wiki/Twitter
chiffre d'affaire 2018: US$ 3.04 billion

- facebook : https://en.wikipedia.org/wiki/Facebook
chiffre d'affaire 2018: US$ 55.838 billion
https://www.nasdaq.com/symbol/fb/financials

- instagram : https://www.businessofapps.com/data/instagram-statistics/
"In 2012 Instagram was acquired by Facebook an estimated $1 billion"

- google : 
 - https://www.statista.com/statistics/266206/googles-annual-global-revenue/
 - https://money.cnn.com/2018/02/01/technology/google-earnings/index.html
"Alphabet (GOOGL), the parent company of Google, reported Thursday that it topped $100 billion in annual sales for the first time in Google's 20-year history."

question: comment ces géants gagnent-ils autant avec des services gratuits???

- uber: https://www.cnbc.com/2019/02/15/uber-2018-financial-results.html
chiffre d'affaire 2018: US$ 11.3 billion

et ainsi de suite

### les méchants

- Cambridge analytica
 - "What is the Cambridge Analytica scandal?" https://www.youtube.com/watch?v=Q91nvbJSmS4

- palantir : https://www.palantir.com
 - "Forget Cambridge Analytica, Palantir has significantly more data on you" : https://www.youtube.com/watch?v=MtY5IL-NaLw

## browsers

- firefox
- chromium
- brave
- tor browser

## mobile

se poser la question: pourquoi cette application a besoin d'avoir accès à mes contacts?

## attitude alternative

constat: les géant de l'informatique "end-user" ont le pouvoir qu'ils ont parce qu'ils se basent tous sur une idée toute simple, à savoir que le plus grand nombre ne s'intéresse pas à l'informatique, qu'il ne veut/peut pas comprendre comment ça marche et que si il a l'impression que c'est gratuit, il n'ira pas plus loin.

En somme, ils prennent comme base:

- la masse est ignorante;
- on peut s'enrichir sur cette ignorance;
- aucun intérêt pour la formation ou l'éducation, qui rend moins ignorant;
- (société de contrôle, voir deleuze) en formatant les besoins au travers d'une offre qui s'auto-ajuste, on peut influer sur la masse -> projet politique de l'économie globalisée;
- priorité au confort, installer la masse dans une passivité active, donner l'impression d'une action sans engagement réel et sans risque de conséquences;
- au travers de la pub (twitter EST un organe publicitaire), se montrer comme novateur socialement;
- créer des identités fortes autour de leur marque
- manichéisme basique, on est dedans ou dehors, pas d'entredeux -> impossible d'accéder au flux facebook sans compte, accès en lecture
- intégrer les alternatives pour concentrer les solutions;
- monopoliser un maximum;
- mise en place d'un langage expert (jargon) dès qu'il s'agit d'accès aux données ou de configuration -> juridique pour les conditions d'utilisation, technique pour les concepts;
- mise en place d'un mythe de l'informatique d'couragent les masses en jouant sur l'ignorance que eux-même instituent;
- masquer les alternatives
- rendre compliqué leur utilisation;
- blocker l'utilisation d'alternatives via des patents (codecs de compression sans information sur l'encodage, formats de fichiers propriétaires, etc.);
- assurer l'ignorance des politiques;
- ne pas se présenter comme idéologique;
- théorie du fun: l'utilisation des solutions doit mettre l'accent sur le confort et la bonne humeur - les photos de chats sont favorisées, à l'opposé des discours politiques, en se défendant d'ingérence:
 - il s'agit d'un espace privé et non public
 - en défendant que "c'est ce que les gens veulent voir"
- recours massifs à l'audima;
- ultra-valorisation de la popularité, CLOUT [being famous and having influence](https://www.urbandictionary.com/define.php?term=clout), ce qui permet d'avoir des statistiques appuyant le "ce que les gens veulent voir";
- contrôle masqué et conformation sociale, tout comme dans le "mall" américain, qui isole l'espace interne de la société:
 - pas de fenêtre, 
 - contrôle à l'entrée, 
 - service d'ordre interne assurant un minimum de présence policière donc de l'état dans l'espace interne,
 - ce qui assure la souveraineté de la société à l'intérieur de la solution




## gratuité

un programme gratuit != libre

- dégoogleiser internet: https://framasoft.org

## reources

- https://www.omidyargroup.com/wp-content/uploads/2017/10/Social-Media-and-Democracy-October-5-2017.pdf

- "Gilles Deleuze : l'art et les sociétés de contrôle", 1987, https://www.youtube.com/watch?v=4ybvyj_Pk7M - dans une société de l'information, "l'information, c'est exactement le système du contrôle"
- "Bernard Stiegler : L'urgence de ralentir", https://www.youtube.com/watch?v=uoDDPSShxqM

